package products;

import java.util.Objects;

public class Product {

    private final Id id;
    private final ProductName name;
    private final ProductType type;
    private final Brand brand;

    public Product(Id id, ProductType type, Brand brand, ProductName name) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.brand = brand;
    }

    public Id id() {
        return id;
    }

    public ProductName name() {
        return name;
    }

    public ProductType type() {
        return type;
    }

    public Brand brand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                type == product.type &&
                brand == product.brand;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, brand);
    }
}
