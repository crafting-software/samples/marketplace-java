package products;

import java.util.Objects;

public class ProductName {

    private final String value;

    private ProductName(String value) {
        this.value = valid(value);
    }

    public static ProductName of(String value) {
        return new ProductName(value.toUpperCase());
    }

    private String valid(String value) {
        return value.substring(0, Integer.min(30, value.length())).trim();
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductName)) return false;
        ProductName name = (ProductName) o;
        return Objects.equals(value, name.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
