package products;

public final class ProductBuilder {
    private Id id;
    private ProductName name;
    private ProductType type;
    private Brand brand;

    private ProductBuilder() {
    }

    public static ProductBuilder aProduct() {
        return new ProductBuilder();
    }

    public ProductBuilder withRandomId() {
        this.id = Id.create();
        return this;
    }

    public ProductBuilder withId(Id id) {
        this.id = id;
        return this;
    }

    public ProductBuilder withName(String name) {
        this.name = ProductName.of(name);
        return this;
    }

    public ProductBuilder withType(ProductType type) {
        this.type = type;
        return this;
    }

    public ProductBuilder withBrand(Brand brand) {
        this.brand = brand;
        return this;
    }

    public Product build() {
        return new Product(id, type, brand, name);
    }
}
