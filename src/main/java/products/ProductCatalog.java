package products;

public interface ProductCatalog {

    Product create(Product product);

    Product searchWith(ProductName name);

    Product searchWith(Id id);
}
