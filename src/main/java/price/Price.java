package price;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

public class Price {

    private final BigDecimal value;
    private final Currency currency;

    private Price(BigDecimal value) {
        this.value = value;
        this.currency = Currency.getInstance(Locale.getDefault());
    }

    public static Price of(BigDecimal value) {
        return new Price(value);
    }

    public static Price of(String value) {
        return new Price(new BigDecimal(value));
    }

    @Override
    public String toString() {
        return NumberFormat.getCurrencyInstance(Locale.getDefault()).format(value.doubleValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Price)) return false;
        Price price = (Price) o;
        return Objects.equals(value, price.value) &&
                Objects.equals(currency, price.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }
}
