package cart;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import shop.ShopName;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Cart {

    private final ImmutableList<Parcel> parcels;

    public Cart() {
        this.parcels = ImmutableList.of();
    }

    private Cart(List<Parcel> parcels) {
        this.parcels = ImmutableList.copyOf(parcels);
    }

    public Cart add(Parcel parcel) {
        final ArrayList<Parcel> parcels = Lists.newArrayList(this.parcels);
        parcels.add(parcel);
        return new Cart(parcels);
    }

    public Optional<Parcel> parcelOf(ShopName shopName) {
        return parcels.stream().filter(p -> p.shop().equals(shopName)).findFirst();
    }

    public List<Parcel> parcels() {
        return parcels;
    }

}
