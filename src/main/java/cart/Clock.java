package cart;

import java.time.LocalDate;

public interface Clock {

    LocalDate now();
}
