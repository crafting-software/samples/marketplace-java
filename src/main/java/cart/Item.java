package cart;

import shop.adverts.Advert;

import java.time.LocalDate;

public class Item {

    private final Advert advert;
    private final LocalDate purchaseDate;
    //quantity

    public Item(Advert advert, LocalDate purchaseDate) {
        this.advert = advert;
        this.purchaseDate = purchaseDate;
    }

    public LocalDate purchaseDate() {
        return purchaseDate;
    }


    public Advert advert() {
        return advert;
    }
}
