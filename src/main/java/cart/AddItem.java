package cart;

import shop.adverts.Advert;
import shop.adverts.AdvertUnavailableException;
import shop.adverts.Inventory;
import utils.markers.UseCase;

@UseCase
public class AddItem {

    private final Clock clock;
    private final Inventory inventory;

    public AddItem(Clock clock, Inventory inventory) {
        this.clock = clock;
        this.inventory = inventory;
    }

    public Cart add(Advert advert, Cart cart) throws AdvertUnavailableException {
        inventory.reserve(advert.id());

        final Parcel parcel = cart.parcelOf(advert.shopName()).orElse(
                Parcel.create(advert.shopName()));

        return cart.add(parcel.add(advert, clock.now()));
    }
}
