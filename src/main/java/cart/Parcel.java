package cart;

import com.google.common.collect.ImmutableList;
import shop.ShopName;
import shop.adverts.Advert;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Parcel {

    private final ShopName shop;
    private final ImmutableList<Item> items;

    private Parcel(ShopName shop) {
        this.shop = shop;
        this.items = ImmutableList.of();
    }

    private Parcel(ShopName shop, List<Item> items) {
        this.shop = shop;
        this.items = ImmutableList.copyOf(items);
    }

    public static Parcel create(ShopName shopName) {
        return new Parcel(shopName);
    }

    public ShopName shop() {
        return shop;
    }

    public Parcel add(Advert advert, LocalDate purchaseDate) {
        final Item item = new Item(advert, purchaseDate);
        final ArrayList<Item> items = new ArrayList<>(this.items);
        items.add(item);
        return new Parcel(shop, items);
    }

    public List<Item> items() {
        return items;
    }
}
