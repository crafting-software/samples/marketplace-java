package shop;

import shop.adverts.Advert;

import java.util.ArrayList;
import java.util.List;

public final class ShopBuilder {
    private Id id;
    private ShopName name;
    private Location location;
    private List<Advert> adverts = new ArrayList<>();

    private ShopBuilder() {
    }

    public static ShopBuilder aShop() {
        return new ShopBuilder();
    }

    public ShopBuilder withName(String name) {
        this.name = ShopName.of(name);
        return this;
    }

    public ShopBuilder withLocation(Location location) {
        this.location = location;
        return this;
    }

    public Shop build() {
        return new Shop(id == null ? Id.random() : id, location, name, adverts);
    }

    public ShopBuilder withAdvert(Advert advert) {
        this.adverts.add(advert);
        return this;
    }
}
