package shop;

import java.util.Objects;
import java.util.UUID;

public class Id {

    private final UUID value;

    private Id() {
        value = UUID.randomUUID();
    }

    private Id(UUID value) {
        this.value = value;
    }

    public static Id random() {
        return new Id();
    }

    public static Id of(UUID value) {
        return new Id(value);
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Id)) return false;
        Id name = (Id) o;
        return Objects.equals(value, name.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
