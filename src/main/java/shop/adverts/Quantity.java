package shop.adverts;

import java.util.Objects;

public class Quantity {

    private Integer value;

    private Quantity(Integer value) {
        if (value < 0) {
            throw new IllegalArgumentException("A quantity should be positive");
        }
        this.value = value;
    }

    public static Quantity of(Integer value) {
        return new Quantity(value);
    }

    public Quantity add(Quantity quantity) {
        return of(quantity.value + value);
    }

    public Quantity minus(Quantity quantity) {
        return of(value - quantity.value);
    }

    public Integer value() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Quantity)) return false;
        Quantity stock = (Quantity) o;
        return Objects.equals(value, stock.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public boolean positive() {
        return value > 0;
    }
}
