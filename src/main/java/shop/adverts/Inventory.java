package shop.adverts;

import utils.markers.Repository;

@Repository
public interface Inventory {

    void store(AdvertId advertId, Quantity quantity);

    Stock reserve(AdvertId advertId) throws AdvertUnavailableException;

    Stock reserve(AdvertId advertId, Quantity quantity) throws AdvertUnavailableException;
}
