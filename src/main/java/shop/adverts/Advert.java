package shop.adverts;

import price.Price;
import products.ProductName;
import shop.ShopName;

import java.util.Objects;

public class Advert {

    private final AdvertId id;
    private final ShopName shopName;
    private final ProductName product;
    private final Price price;

    public Advert(ProductName product, ShopName shopName, Price price) {
        this.product = product;
        this.shopName = shopName;
        this.price = price;
        this.id = AdvertId.create();
    }

    public ProductName product() {
        return product;
    }

    public Price price() {
        return price;
    }

    public ShopName shopName() {
        return shopName;
    }

    public AdvertId id() {
        return this.id;
    }

    @Override
    public String toString() {
        return product + " sold by " + shopName + " for " + price ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Advert)) return false;
        Advert advert = (Advert) o;
        return Objects.equals(id, advert.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
