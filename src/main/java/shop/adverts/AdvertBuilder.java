package shop.adverts;

import price.Price;
import products.ProductName;
import shop.ShopName;

public final class AdvertBuilder {

    private ProductName product;
    private ShopName shopName;
    private Price price;

    public static AdvertBuilder anAdvertFrom(ShopName shopName) {
        return new AdvertBuilder().withShopName(shopName);
    }

    public static AdvertBuilder anAdvertFrom(String shopName) {
        return new AdvertBuilder().withShopName(ShopName.of(shopName));
    }

    private AdvertBuilder on(ProductName product) {
        this.product = product;
        return this;
    }

    public AdvertBuilder on(String product) {
        return on(ProductName.of(product));
    }

    private AdvertBuilder at(Price price) {
        this.price = price;
        return this;
    }

    public AdvertBuilder at(String price) {
        return at(Price.of(price));
    }

    private AdvertBuilder withShopName(ShopName shopName) {
        this.shopName = shopName;
        return this;
    }

    public Advert build() {
        return new Advert(product, shopName, price);
    }
}
