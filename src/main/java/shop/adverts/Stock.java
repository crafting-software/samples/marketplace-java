package shop.adverts;

import java.util.Objects;

public class Stock {

    private final AdvertId advertId;
    private Quantity quantity;

    private Stock(Quantity quantity, AdvertId advertId) {
        this.quantity = quantity;
        this.advertId = advertId;
    }

    public static Stock of(AdvertId advertId, Quantity quantity) {
        return new Stock(quantity, advertId);
    }

    public AdvertId advertId() {
        return advertId;
    }

    public boolean left() {
        return quantity.positive();
    }

    public Stock store(Quantity quantity) {
        return of(advertId, this.quantity.add(quantity));
    }

    public Stock reserve(Quantity quantity) {
        return of(advertId, this.quantity.minus(quantity));
    }

    @Override
    public String toString() {
        return quantity.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stock)) return false;
        Stock stock = (Stock) o;
        return Objects.equals(quantity, stock.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity);
    }
}
