package shop.adverts;

import java.util.Objects;
import java.util.UUID;

public class AdvertId {

    private final UUID value;

    private AdvertId() {
        value = UUID.randomUUID();
    }

    private AdvertId(UUID value) {
        this.value = value;
    }

    public static AdvertId create() {
        return new AdvertId();
    }

    public static AdvertId of(UUID value) {
        return new AdvertId(value);
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdvertId)) return false;
        AdvertId name = (AdvertId) o;
        return Objects.equals(value, name.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
