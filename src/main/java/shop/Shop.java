package shop;

import com.google.common.collect.ImmutableList;
import products.ProductName;
import shop.adverts.Advert;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Shop {

    private final Id id;
    private final ShopName name;
    private final Location location;
    private final ImmutableList<Advert> adverts;

    public Shop(Id id, Location location, ShopName name, List<Advert> adverts) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.adverts = ImmutableList.copyOf(adverts);
    }

    public Id id() {
        return id;
    }

    public ShopName name() {
        return name;
    }

    public Location location() {
        return location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shop)) return false;
        Shop shop = (Shop) o;
        return Objects.equals(id, shop.id) &&
                Objects.equals(name, shop.name) &&
                location == shop.location;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, location);
    }

    public Optional<Advert> advert(ProductName p) {
        return adverts.stream().filter(a -> a.product().equals(p)).findFirst();
    }

    public Shop add(Advert advert) {
        final ArrayList<Advert> copy = new ArrayList<>(this.adverts);
        copy.add(advert);
        return new Shop(id, location, name, copy);
    }
}
