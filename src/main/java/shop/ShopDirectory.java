package shop;

import java.util.Optional;

public interface ShopDirectory {

    Shop create(Shop shop);

    Optional<Shop> searchWith(ShopName name);

    Optional<Shop> searchWith(Id id);
}
