package shop;

import java.util.Objects;

public class ShopName {

    private final String value;

    private ShopName(String value) {
        this.value = valid(value);
    }

    public static ShopName of(String value) {
        return new ShopName(value.toUpperCase());
    }

    private String valid(String value) {
        return value.substring(0, Integer.min(30, value.length()));
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShopName)) return false;
        ShopName name = (ShopName) o;
        return Objects.equals(value, name.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
