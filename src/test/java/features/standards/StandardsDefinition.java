package features.standards;

import cucumber.api.java.en.Then;
import org.assertj.core.api.Assertions;
import products.ProductCatalog;
import products.ProductName;
import shop.ShopName;
import shop.ShopDirectory;

public class StandardsDefinition {

    private final ShopDirectory shopDirectory;
    private final ProductCatalog productCatalog;

    public StandardsDefinition(ShopDirectory shopDirectory, ProductCatalog productCatalog) {
        this.shopDirectory = shopDirectory;
        this.productCatalog = productCatalog;
    }

    @Then("shop (.*) should exists")
    public void shopShouldExists(String name) {
        Assertions.assertThat(shopDirectory.searchWith(ShopName.of(name))).isNotNull();
    }

    @Then("product (.*) should exists")
    public void productShouldExists(String name) {
        Assertions.assertThat(productCatalog.searchWith(ProductName.of(name))).isNotNull();
    }
}
