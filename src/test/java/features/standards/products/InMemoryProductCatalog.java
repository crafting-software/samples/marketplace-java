package features.standards.products;

import products.Id;
import products.ProductName;
import products.Product;
import products.ProductCatalog;

import java.util.HashMap;

public class InMemoryProductCatalog implements ProductCatalog {

    private HashMap<Id, Product> mapById = new HashMap<>();
    private HashMap<ProductName, Product> mapByName = new HashMap<>();

    @Override
    public Product create(Product product) {
        mapById.put(product.id(), product);
        mapByName.put(product.name(), product);
        return product;
    }

    @Override
    public Product searchWith(ProductName name) {
        return mapByName.get(name);
    }

    @Override
    public Product searchWith(Id id) {
        return mapById.get(id);
    }
}
