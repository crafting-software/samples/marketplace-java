package features.standards.products;

import products.Brand;
import products.Product;
import products.ProductBuilder;
import products.ProductType;

public class ProductSupplier {

    static Product oneplus6T() {
        return ProductBuilder.aProduct()
                .withName("Oneplus 6T 128 Go")
                .withBrand(Brand.ONEPLUS)
                .withType(ProductType.PHONE).build();
    }
}