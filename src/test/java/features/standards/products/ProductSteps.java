package features.standards.products;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.assertj.core.api.Assertions;
import products.Brand;
import products.ProductName;
import products.Product;
import products.ProductType;

public class ProductSteps {

    private final InMemoryProductCatalog productCatalog;
    private Product current;

    public ProductSteps(InMemoryProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    @Given("^Oneplus 6T 128 Go product exists$")
    public void oneplusTGoProductExists() {
        final Product product = productCatalog.create(ProductSupplier.oneplus6T());
        current = productCatalog.searchWith(product.name());
    }

    @Then("it should be named (.*)$")
    public void itShouldBeNamedOneplusTGo(String name) {
        Assertions.assertThat(current.name()).isEqualTo(ProductName.of(name));
    }

    @Then("^its type should be (.*)$")
    public void itsTypeShouldBePHONE(ProductType type) {
        Assertions.assertThat(current.type()).isEqualTo(type);
    }

    @Then("^it should be branded (.*)$")
    public void itShouldBeBrandedONEPLUS(Brand brand) {
        Assertions.assertThat(current.brand()).isEqualTo(brand);
    }
}
