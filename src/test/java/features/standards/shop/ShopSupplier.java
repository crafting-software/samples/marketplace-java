package features.standards.shop;

import shop.Location;
import shop.Shop;
import shop.ShopBuilder;
import shop.adverts.Advert;
import shop.adverts.AdvertBuilder;
import shop.adverts.Quantity;
import shop.adverts.Stock;

public class ShopSupplier {

    private final InMemoryInventory inMemoryInventory;

    public ShopSupplier(InMemoryInventory inMemoryInventory) {
        this.inMemoryInventory = inMemoryInventory;
    }

    Shop phoneAndGame() {
        final Advert oneplusAdvert = AdvertBuilder.anAdvertFrom("Phone&Game")
                .on("Oneplus 6T 128 Go")
                .at("595.00")
                .build();

        inMemoryInventory.store(oneplusAdvert.id(), Quantity.of(100));

        return
                ShopBuilder.aShop()
                        .withName("Phone&Game")
                        .withLocation(Location.PARIS)
                        .withAdvert(oneplusAdvert)
                        .build();
    }
}