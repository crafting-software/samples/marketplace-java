package features.standards.shop;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.assertj.core.api.Assertions;
import shop.Location;
import shop.ShopName;
import shop.Shop;
import shop.adverts.Advert;
import shop.adverts.Stock;

import java.util.List;
import java.util.Optional;



public class ShopSteps {

    private Shop current;
    private final ShopSupplier shopSupplier;
    private final InMemoryShopDirectory shopDirectory;
    private InMemoryInventory inMemoryInventory;

    public ShopSteps(ShopSupplier shopSupplier, InMemoryShopDirectory shopDirectory, InMemoryInventory inMemoryInventory) {
        this.shopSupplier = shopSupplier;
        this.shopDirectory = shopDirectory;
        this.inMemoryInventory = inMemoryInventory;
    }


    @Given("^Phone&Game$")
    public void phoneGame() {
        final Shop shop = shopDirectory.create(shopSupplier.phoneAndGame());
        current = shopDirectory.searchWith(shop.name()).orElseThrow(() -> new IllegalStateException("Phone&Game shop has not been created"));
    }

    @Then("^shop should be named (.*)$")
    public void shopShouldBeNamedPhoneGame(String name) {
        Assertions.assertThat(current.name()).isEqualTo(ShopName.of(name));
    }

    @Then("^shop should be located at (.*)$")
    public void shopShouldBeLocatedAtPARIS(Location location) {
        Assertions.assertThat(current.location()).isEqualTo(location);
    }

    @Then("^adverts should be :$")
    public void advertsShouldBe(List<AdvertInfo> adverts) {
        adverts.forEach(advertInfo -> {
            final Optional<Advert> advert = current.advert(advertInfo.product());
            Assertions.assertThat(advert).isPresent();
            Assertions.assertThat(render(advert.get())).isEqualTo(render(advertInfo));
        });
    }

    private String render(Advert a) {
        final Stock stock = inMemoryInventory.getStock(a.id());
        return a.product() + " sold by " + a.shopName() + " for " + a.price() + " with a stock of " + stock;
    }


    private String render(AdvertInfo a) {
        return a.product() + " sold by " + a.shopName() + " for " + a.price() + " with a stock of " + a.stock();
    }

}
