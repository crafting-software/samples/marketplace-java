package features.standards.shop;

import price.Price;
import products.ProductName;
import shop.ShopName;
import shop.adverts.Quantity;
import shop.adverts.Stock;

public class AdvertInfo {

    private final ProductName product;
    private final ShopName shopName;
    private final Price price;
    private final Quantity stock;

    public AdvertInfo(ProductName product, ShopName shopName, Price price, Quantity stock) {
        this.product = product;
        this.shopName = shopName;
        this.price = price;
        this.stock = stock;
    }

    public ProductName product() {
        return product;
    }

    public ShopName shopName() {
        return shopName;
    }

    public Price price() {
        return price;
    }

    public Quantity stock() {
        return stock;
    }

}
