package features.standards.shop;

import shop.Id;
import shop.ShopName;
import shop.Shop;
import shop.ShopDirectory;

import java.util.HashMap;
import java.util.Optional;

public class InMemoryShopDirectory implements ShopDirectory {

    private HashMap<Id, Shop> mapById = new HashMap<>();
    private HashMap<ShopName, Shop> mapByName = new HashMap<>();

    @Override
    public Shop create(Shop shop) {
        mapById.put(shop.id(), shop);
        mapByName.put(shop.name(), shop);
        return shop;
    }

    @Override
    public Optional<Shop> searchWith(ShopName name) {
        return Optional.ofNullable(mapByName.get(name));
    }

    @Override
    public Optional<Shop> searchWith(Id id) {
        return Optional.ofNullable(mapById.get(id));
    }
}
