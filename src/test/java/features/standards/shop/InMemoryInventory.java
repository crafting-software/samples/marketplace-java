package features.standards.shop;

import shop.adverts.*;

import java.util.HashMap;
import java.util.Map;

public class InMemoryInventory implements Inventory {

    private Map<AdvertId, Stock> stocks = new HashMap<>();

    @Override
    public void store(AdvertId advertId, Quantity quantity) {
        stocks.computeIfPresent(advertId, (id, s) -> s.store(quantity));
        stocks.putIfAbsent(advertId, Stock.of(advertId, quantity));
    }

    @Override
    public Stock reserve(AdvertId advertId) throws AdvertUnavailableException {
        return reserve(advertId, Quantity.of(1));
    }

    @Override
    public Stock reserve(AdvertId advertId, Quantity quantity) throws AdvertUnavailableException {
        Stock stock = get(advertId);

        if (!stock.left()) {
            throw new AdvertUnavailableException();
        }
        stock = stock.reserve(quantity);
        stocks.put(advertId, stock);
        return stock;
    }

    private Stock get(AdvertId id) {
        final Stock stock = stocks.get(id);
        if (stock == null) {
            throw new IllegalArgumentException();
        }
        return stock;
    }

    public Stock getStock(AdvertId advertId) {
        return stocks.get(advertId);
    }
}
