package features.cart;

import cucumber.api.java.en.Then;
import org.assertj.core.api.Assertions;
import products.ProductName;
import shop.ShopName;
import shop.Shop;
import shop.ShopDirectory;
import shop.adverts.Advert;
import shop.adverts.AdvertId;
import shop.adverts.Quantity;
import shop.adverts.Stock;
import features.standards.shop.InMemoryInventory;

public class ReservationSteps {

    public ReservationSteps(ShopDirectory shopDirectory, InMemoryInventory inventory) {
        this.shopDirectory = shopDirectory;
        this.inventory = inventory;
    }

    private ShopDirectory shopDirectory;
    private final InMemoryInventory inventory;

    @Then("^(.*) stock of (.*) should be (.*)$")
    public void phoneGameStockOfOneplusTGoShouldBe(String shopName, String productName, Integer stock) {
        final Stock inventoryStock = inventory.getStock(advertId(ShopName.of(shopName), ProductName.of(productName)));
        Assertions.assertThat(inventoryStock).isEqualTo(Stock.of(inventoryStock.advertId(), Quantity.of(stock)));
    }

    private AdvertId advertId(ShopName shopName, ProductName productName) {
        final Shop shop = shopDirectory.searchWith(shopName).orElseThrow(IllegalArgumentException::new);
        final Advert advert = shop.advert(productName).orElseThrow(IllegalArgumentException::new);
        return advert.id();
    }
}
