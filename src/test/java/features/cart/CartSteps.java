package features.cart;

import cart.AddItem;
import cart.Cart;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import products.ProductName;
import shop.Shop;
import shop.ShopDirectory;
import shop.ShopName;
import shop.adverts.Advert;
import shop.adverts.AdvertUnavailableException;
import features.standards.shop.InMemoryInventory;
import features.utils.LocalClock;

public class CartSteps {

    private final ShopDirectory shopDirectory;
    private Cart cart;
    private final AddItem addItem;

    public CartSteps(ShopDirectory shopDirectory, LocalClock clock, InMemoryInventory inventory) {
        this.shopDirectory = shopDirectory;
        this.addItem = new AddItem(clock, inventory);
    }

    @Given("^I have an empty shopping cart$")
    public void iHaveAnEmptyShoppingCart() {
        cart = new Cart();
    }

    @When("^I add (.*) from (.*) shop to my cart$")
    public void iAddFromPhoneGameShopToMyCart(String productName, String shopName) throws AdvertUnavailableException {
        final Shop shop = shopDirectory.searchWith(ShopName.of(shopName))
                .orElseThrow(() -> new IllegalArgumentException(shopName + "shop has not been created"));
        final Advert advert = shop.advert(ProductName.of(productName))
                .orElseThrow(() -> new IllegalArgumentException(shopName + "shop is not selling " + productName));
        cart = addItem.add(advert, cart);
    }

    @Then("my shopping cart should contain (.*) from (.*)$")
    public void myShoppingCartShouldContainOnly(String productName, String shopName) {
        Assertions.assertThat(cart.parcelOf(ShopName.of(shopName)).get().items().stream().map(i -> i.advert().product())).containsExactly(ProductName.of(productName));
    }
}
