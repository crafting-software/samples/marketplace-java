package features.utils;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true,
        glue = "features",
        features = "src/test/resources/features"

)
public class FeaturesTest {
}
