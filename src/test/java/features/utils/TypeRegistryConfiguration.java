package features.utils;

import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;
import features.standards.shop.AdvertInfo;
import io.cucumber.datatable.DataTableType;
import io.cucumber.datatable.TableEntryTransformer;
import price.Price;
import products.ProductName;
import shop.ShopName;
import shop.adverts.Quantity;

import java.math.BigDecimal;
import java.util.Locale;

import static java.util.Locale.ENGLISH;

@SuppressWarnings("unused")
public class TypeRegistryConfiguration implements TypeRegistryConfigurer {

    public Locale locale() {
        return ENGLISH;
    }

    public void configureTypeRegistry(final TypeRegistry typeRegistry) {
        typeRegistry.defineDataTableType(new DataTableType(AdvertInfo.class, rowToAdvert()));
    }

    private TableEntryTransformer<AdvertInfo> rowToAdvert() {
        return entry ->
                new AdvertInfo(
                        ProductName.of(entry.get("product name")),
                        ShopName.of(entry.get("shop name")), Price.of(new BigDecimal(entry.get("price").replace("€", "").trim())),
                        Quantity.of(Integer.valueOf(entry.get("stock")))
                );
    }
}
