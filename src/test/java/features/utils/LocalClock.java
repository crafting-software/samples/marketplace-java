package features.utils;

import cart.Clock;

import java.time.LocalDate;

public class LocalClock implements Clock {

    @Override
    public LocalDate now() {
        return LocalDate.now();
    }
}