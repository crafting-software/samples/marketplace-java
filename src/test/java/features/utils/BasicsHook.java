package features.utils;

import cucumber.api.java.Before;
import features.standards.products.ProductSteps;
import features.standards.shop.ShopSteps;

public class BasicsHook {

    final private ProductSteps productSteps;
    final private ShopSteps shopSteps ;

    public BasicsHook(ProductSteps productSteps, ShopSteps shopSteps) {
        this.productSteps = productSteps;
        this.shopSteps = shopSteps;
    }

    @Before("@basics")
    public void basics(){
        productSteps.oneplusTGoProductExists();
        shopSteps.phoneGame();
    }
}
