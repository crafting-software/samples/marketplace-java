package unit.cart;

import cart.Parcel;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import shop.ShopName;
import shop.adverts.Advert;
import shop.adverts.AdvertBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ParcelTest {

    private final Parcel parcel = Parcel.create(ShopName.of("a_shop"));

    @Test
    public void item_should_be_added_to_Parcel() {
        final Advert advert = anAdvert();
        final LocalDate purchaseDate = date("2019-04-15 10:30");
        final Parcel parcel = this.parcel.add(advert, purchaseDate);

        Assertions.assertThat(parcel.items().stream()).allMatch(i -> i.advert().equals(advert) && i.purchaseDate().equals(purchaseDate));
    }

    private LocalDate date(String text) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(text, formatter).toLocalDate();
    }

    private Advert anAdvert() {
        return AdvertBuilder.anAdvertFrom(parcel.shop()).at("586.00").on("anyProduct").build();

    }
}