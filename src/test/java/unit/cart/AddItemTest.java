package unit.cart;

import cart.AddItem;
import cart.Cart;
import features.standards.shop.InMemoryInventory;
import features.utils.LocalClock;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import shop.adverts.*;

public class AddItemTest {

    private Cart cart;
    private InMemoryInventory inventory = new InMemoryInventory();
    private Advert addedAdvert;

    @Test
    public void add_an_available_advert_should_create_a_parcel() throws Exception {
        given_an_empty_cart();
        and_iphone_is_selled();
        when_I_add_iphone();
        then_a_parcel_should_be_created();
    }

    @Test
    public void add_an_available_advert_should_reserve_it() throws Exception {
        given_an_empty_cart();
        and_iphone_is_selled();
        when_I_add_iphone();
        then_stock_should_be(99);
    }

    private void then_stock_should_be(int stock) {
        Assertions.assertThat(inventory.getStock(addedAdvert.id())).isEqualTo(Stock.of(addedAdvert.id(), Quantity.of(stock)));
    }

    private void then_a_parcel_should_be_created() {
        Assertions.assertThat(cart.parcels().stream()).allMatch(parcel -> parcel.shop().equals(addedAdvert.shopName()));
    }

    private void when_I_add_iphone() throws AdvertUnavailableException {
        final AddItem addItem = new AddItem(new LocalClock(), inventory);
        addItem.add(addedAdvert, cart);
    }

    private void and_iphone_is_selled() {
        addedAdvert = AdvertBuilder.anAdvertFrom("anyShop").at("586.00").on("iphone").build();
    }

    private void given_an_empty_cart() {
        cart = new Cart();
    }
}