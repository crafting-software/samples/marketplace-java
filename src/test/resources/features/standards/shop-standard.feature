Feature: standard shops definition

  Scenario: Phone&Game
    Given Phone&Game
    Then shop should be named Phone&Game
    Then shop should be located at PARIS
    #Then seller should be able to deliver with UPS, La Poste
    #Then shipping cost should be free from 30 euros order
    Then adverts should be :
      | product name      | price    | stock | shop name  |
      | Oneplus 6T 128 Go | 595.00 € | 100   | Phone&Game |


 #Scenario: Phone101
 #  Given Phone101
 #  Then shop should be named Phone101
 #  Then shop should be located at Paris
 #  #Then seller should be able to deliver with Relais Colis, La Poste
 #  Then adverts should be :
 #    | product name      | price    | stock |
 #    | Oneplus 6T 128 Go | 650.00 € | 1000  |

