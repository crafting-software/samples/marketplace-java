Feature: standard products definition

  Define a set of available products to be able to create scenarios

  Scenario: OnePlus 6T 128 Go
    Given Oneplus 6T 128 Go product exists
    Then it should be named Oneplus 6T 128 Go
    Then its type should be PHONE
    Then it should be branded ONEPLUS