Feature: cart

  @basics
  Scenario: adding first item

    Given I have an empty shopping cart
    When I add Oneplus 6T 128 Go from Phone&Game shop to my cart
    Then my shopping cart should contain Oneplus 6T 128 Go from Phone&Game